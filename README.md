## Desafio

Utilize o código disponibilizado dentro da pasta `desafios` deste diretório para executar os desafios a seguir.
Execute `yarn install && yarn start` para iniciar o app.

### Component SimpleCount

[x] Complete o component `SimpleCount` para fazer com que ele conte toda vez que o botão é clicado

### Component Timer

[x] Complete o component `Timer` para que a cada segundo o `time` seja incrementado

### Component FastState

[x] Altere no máximo 3 linhas do component `FastState` para que a diferença entre os valores mostrados `seja?` menor ou igual a 1
