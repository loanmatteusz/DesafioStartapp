import React, { useState } from 'react';
import { useEffect, useRef } from 'react';

function FastState() {
  const [value, setValue] = useState(0);
  const valueWithRef = useRef(0);

  useEffect(() => {
    const intervalId = setInterval(() => {
        setTimeout(() => {
            setValue(value + 1);
            valueWithRef.current += Math.abs(value - valueWithRef.current) <= 1 ? 1 : 0;
        }, 10);
    }, 1000);
    return () => clearInterval(intervalId);
  }, [value, setValue]);

  return (
    <div>
        <p>
          Fast State: {value}
          <br />
          Fast State Correct: {valueWithRef.current}
        </p>
    </div>
  );
}

export default FastState;
